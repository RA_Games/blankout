﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class CameraZoom : MonoBehaviour {

    [SerializeField]PixelPerfectCamera[] cameras;

    public float speed = 1;

    private bool isZooming;

    private Vector2[] resolutions;
    private Vector2[] newResolutions;

    private void Start()
    {
        resolutions = new Vector2[cameras.Length];

        for (int i = 0; i < cameras.Length; i++)
        {
            resolutions[i].x = cameras[i].refResolutionX;
            resolutions[i].y = cameras[i].refResolutionY;
        }
    }

    void Update ()
    {
        if (isZooming)
        {
            int i = 0;
            bool[] ready = new bool[cameras.Length];

            foreach (PixelPerfectCamera cam in cameras)
            {
                cam.refResolutionX = (int)Mathf.Lerp(cam.refResolutionX, newResolutions[i].x, Time.deltaTime*speed);
                cam.refResolutionY = (int)Mathf.Lerp(cam.refResolutionY, newResolutions[i].y, Time.deltaTime*speed);

                if(Vector2.Distance(new Vector2(cam.refResolutionX, cam.refResolutionY), new Vector2(newResolutions[i].x, newResolutions[i].y)) < 2)
                {
                    ready[i] = true;
                }

                i++;
            }

            for (int index = 0; index < ready.Length; index++)
            {
                if (!ready[index])
                    return;
            }

            i = 0;

            foreach (PixelPerfectCamera cam in cameras)
            {
                cam.refResolutionX = (int)newResolutions[i].x;
                cam.refResolutionY = (int)newResolutions[i].y;
                i++;
            }

            isZooming = false;
        }
	}

    public void ZoomOut(int zoom)
    {
        newResolutions = new Vector2[resolutions.Length];

        for (int i = 0; i < resolutions.Length; i++)
        {
            newResolutions[i].x = resolutions[i].x * zoom;
            newResolutions[i].y = resolutions[i].y * zoom;
        }

        isZooming = true;
    }

    public void ZoomIn(int zoom)
    {
        newResolutions = new Vector2[resolutions.Length];

        for (int i = 0; i < resolutions.Length; i++)
        {
            newResolutions[i].x = resolutions[i].x / zoom;
            newResolutions[i].y = resolutions[i].y / zoom;
        }

        isZooming = true;
    }
}
