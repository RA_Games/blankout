﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour {

    [SerializeField] MyController controller;

    [SerializeField] CharacterController2D characterController;

    private float _horizontalMovement;

    public float speed = 1f;
    private bool _isJump;
    private bool _isCrouch;

    private bool _lockMovement;

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void Lock()
    {
        _lockMovement = true;
        characterController.Stop();

    }

    public void Unlock()
    {
        _lockMovement = false;
    }

    public void Teleport(Transform point)
    {
        transform.position = point.position;
    }

    void Update ()
    {
        if (!_lockMovement)
        {
            _horizontalMovement = controller.GetHorizontalAxis().GetAxisRaw() * speed;

            if (controller.Jump().GetKeyDown())
            {
                Jump();
            }
        }
	}

    public void Jump()
    {
        _isJump = true;
    }

    private void FixedUpdate()
    {
        characterController.Move(_horizontalMovement * Time.fixedDeltaTime, _isCrouch, _isJump);

        _isJump = false;
    }
}
