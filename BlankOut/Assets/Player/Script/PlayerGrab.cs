﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PlayerGrab : MonoBehaviour {

    [SerializeField] MyController controller;

    [SerializeField] Animator anim;

    [SerializeField] GameObject action_button;
    [SerializeField] Rigidbody2D playerRB;
    [SerializeField] Transform smallBox_Pos;
    [SerializeField] Transform bigBox_Pos;
    [SerializeField] BoxCollider2D detector_collider;
    [SerializeField] BoxCollider2D bigBox_Collider;
    [SerializeField] BoxCollider2D smallBox_Collider;
    private float originalWeight;

    private Box currentBox;

    bool grabStatus;

    private void Awake()
    {
        originalWeight = playerRB.mass; 
    }

    private void Grab(Box box)
    {
        grabStatus = !anim.GetBool("IsGrabbing");

        anim.SetBool("IsGrabbing", grabStatus);


        if (grabStatus)
        {
            anim.SetFloat("Weight", box.Weight);
            anim.SetFloat("Relative_Target", box.transform.position.x - transform.position.x);

            playerRB.mass = originalWeight + box.Weight;

            //box.Rb.isKinematic = true;
            box.Rb.simulated = false;

            box.GetComponent<Collider2D>().enabled = false;

            if (box is BigBox)
            {
                box.transform.SetParent(bigBox_Pos.transform);
                box.transform.position = bigBox_Pos.transform.position;

                bigBox_Collider.enabled = true;

                anim.SetBool("BigBoxGrab", true);

                playerRB.GetComponent<CharacterController2D>().PreventFlip(true);
            }
            else
            {
                box.transform.SetParent(smallBox_Pos.transform);
                box.transform.position = smallBox_Pos.transform.position;

                smallBox_Collider.enabled = true;
            }
            
            detector_collider.enabled = false;
        }
        else
        {
            if (box is BigBox)
            {
                playerRB.GetComponent<CharacterController2D>().PreventFlip(false);
            }

            anim.SetBool("BigBoxGrab", false);
            anim.SetFloat("Weight", 0);
            anim.SetFloat("Relative_Target", 0);
            playerRB.mass = originalWeight;

            //box.Rb.isKinematic = false;
            box.Rb.simulated = true;

            box.transform.SetParent(null);
            box.GetComponent<Collider2D>().enabled = true;
            bigBox_Collider.enabled = false;
            smallBox_Collider.enabled = false;
            currentBox = null;

            detector_collider.enabled = true;
        }
    }

    private void Update()
    {
        if (currentBox != null)
        {
            if (controller.Action().GetKeyDown())
            {
                Grab(currentBox);
            }
        }
 

        if (currentBox != null && !grabStatus)
        {
            action_button.SetActive(true);
        }
        else
        {
            action_button.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        currentBox = collision.GetComponent<Box>();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!anim.GetBool("IsGrabbing"))
        {
            currentBox = null;
        }
    }
}
