﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notifier : MonoBehaviour
{

    [SerializeField] Animator anim;

    int curretnDevices;

    private void Start()
    {
        curretnDevices = GetJoystickDevices();
    }

    private void Update()
    {
        int lastDevices = GetJoystickDevices();

        if (lastDevices != curretnDevices)
        {
            if (lastDevices < curretnDevices)
            {
                anim.SetTrigger("Controller Disconnected");
            }
            else if (lastDevices > curretnDevices)
            {
                anim.SetTrigger("Controller Connected");
            }

            curretnDevices = lastDevices;
        }
    }

    private int GetJoystickDevices()
    {
        int devices = 0;

        for (int i = 0; i < Input.GetJoystickNames().Length; i++)
        {
            if (Input.GetJoystickNames()[i].Length > 0)
            {
                devices++;
            }
        }

        return devices;
    }
}