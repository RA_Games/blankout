﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Pedestal : MonoBehaviour
{
    private SmallBox selectedBox;
    private BoxCollider2D m_collider;

    [SerializeField] private Animator pedestal_anim;
    [SerializeField] private Animator figure_anim;

    private BigBox[] linkedBoxes;

    public enum Figure
    {
        None, Square, Triangle, Visual, Diamond, Infinite
    }

    public Figure figure;

    void Start ()
    {
        m_collider = GetComponent<BoxCollider2D>();

        figure_anim.SetTrigger(figure.ToString());
    }

    private void OnEnable()
    {
        linkedBoxes = FindObjectsOfType<BigBox>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (selectedBox == null && collision.gameObject.GetComponent<EnergyPlant>() == null)
        {
            try
            {
                SmallBox current = collision.gameObject.GetComponent<SmallBox>();

                pedestal_anim.SetBool("Active", true);

                selectedBox = current;

                foreach (BigBox box in linkedBoxes)
                {
                    if (box.figure == figure)
                    {
                        box.currentCharge = selectedBox.currentCharge;
                        box.ChangueMaterial(selectedBox.currentMaterial);
                    }
                }
            }
            catch { }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        SmallBox current = collision.gameObject.GetComponent<SmallBox>();

        if (current == selectedBox && collision.gameObject.GetComponent<EnergyPlant>() == null)
        {
            pedestal_anim.SetBool("Active", false);

            foreach (BigBox box in linkedBoxes)
            {
                if (box.figure == figure)
                {
                    box.currentCharge = box.originalCharge;
                    box.ChangueMaterial(box.originalMaterial);
                }
            }

            selectedBox = null;
        }
    }
}
