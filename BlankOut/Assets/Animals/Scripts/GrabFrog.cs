﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabFrog : MonoBehaviour {

    [SerializeField] GameObject animalSpot;
    [SerializeField] MyController controller;

    [SerializeField] Animal frog;

    public UnityEngine.Events.UnityEvent onGrab;


    private void Update()
    {
        if (controller.Action().GetKeyDown())
        {
            if (frog != null)
            {
                animalSpot.SetActive(true);
                frog.gameObject.SetActive(false);
                gameObject.SetActive(false);
                onGrab.Invoke();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        frog = collision.GetComponent<Animal>();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        frog = null;
    }
}
