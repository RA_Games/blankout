﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Animal : MonoBehaviour {

    public bool isMoving;
    [SerializeField] Animator m_anim;
    [SerializeField] SpriteRenderer m_renderer;
    [SerializeField] protected AudioSource m_audio;

    public float walkDistance = 1;
    public float speed = 1;
    public enum Direction { Left = -1, Right = 1 }
    public Direction direction = Direction.Right;

    protected Vector2 nextPos;

    private void Start()
    {
        if (isMoving)
            MoveAnimal();
    }

    public void InvertDirection()
    {
        int d = (int)direction * -1;
        direction = (Direction)d;

        if(d > 0)
        {
            m_renderer.flipX = false;
        }
        else
        {
            m_renderer.flipX = true;
        }
    }

    public void MoveAnimal()
    {
        isMoving = true;
        nextPos = transform.position;
        m_anim.SetBool("Moving", true);
    }

    public void StopMoveAnimal()
    {
        isMoving = false;
        m_anim.SetBool("Moving", false);

    }
    public abstract void Move();
}
