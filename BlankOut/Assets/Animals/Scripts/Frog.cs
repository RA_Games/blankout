﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : Animal
{
    public override void Move()
    {
        m_audio.Play();
        nextPos = transform.position + (new Vector3(walkDistance * (int)direction, 0, 0));
    }

    private void Update()
    {
        if (isMoving)
        {
            transform.position = Vector2.MoveTowards(transform.position, nextPos, Time.deltaTime * speed);
        }
    }
}
