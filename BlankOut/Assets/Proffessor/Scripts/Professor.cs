﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Professor : MonoBehaviour {

    public Vector3 currentSpeed;

    private Vector3 lastPos;

    public Animator anim;

    private void Start()
    {
        lastPos = transform.position;
    }

    void Update () {

        currentSpeed = (lastPos - transform.position) / Time.deltaTime;

        anim.SetFloat("Speed", currentSpeed.magnitude);

        lastPos = transform.position;
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(Professor))]
public class ProfessorEditor : Editor
{
    Professor myTarget;

    private void OnEnable()
    {
        myTarget = (Professor)target;
    }

    public override void OnInspectorGUI()
    {
        myTarget.anim = (Animator)EditorGUILayout.ObjectField("Animator" , myTarget.anim, typeof(Animator), true);

        EditorGUILayout.LabelField("Speed: " + myTarget.currentSpeed.magnitude + " m/s");
    }
}
#endif
