﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Node Engine/Novel/Actor")]
public class Actor : ScriptableObject {

    public string actor_name;
	
}
