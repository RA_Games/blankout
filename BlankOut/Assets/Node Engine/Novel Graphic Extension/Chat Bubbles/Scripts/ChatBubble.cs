﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatBubble : MonoBehaviour {

    public Actor actor;
    public MyController myController;
    public Vector2 offset;
    public Transform bubble_target;
    public RectTransform bubble_rect;
    public Text textComponent;
    public Animator bubble_animator;
    public AudioSource typeSound;
    public float speed = 0.1f;

    public bool autoSkip = true;
    private bool writingFinish;
    Action OnFinishRead;

    public static ChatBubble GetBubbleInScene(Actor actor)
    {
        foreach (ChatBubble bubble in FindObjectsOfType<ChatBubble>())
        {
            if (actor == bubble.actor)
                return bubble;
        }

        return null;
    }

    private void OnValidate()
    {
        Vector3 bubblePos = transform.position;
        bubblePos.x += offset.x;
        bubblePos.y = offset.y;
        bubble_target.position = bubblePos;
    }

    public void Write(string text, Action OnFinishRead)
    {
        this.OnFinishRead = OnFinishRead;
        StopAllCoroutines();
        StartCoroutine(WriteDialogue(text));
    }

    public void Write(string[] text, Action OnFinishRead)
    {
        this.OnFinishRead = OnFinishRead;
        StopAllCoroutines();
        StartCoroutine(WriteDialogues(text));
    }

    private IEnumerator WriteDialogues(string[] messages)
    {
        int index = 0;

        bubble_animator.SetBool("IsShowing", true);

        for (int i = 0; i < messages.Length; i++)
        {
            writingFinish = false;

            textComponent.text = string.Empty;

            bubble_animator.SetBool("IsWriting", true);

            yield return new WaitForSeconds(1);

            yield return StartCoroutine(TypeText(messages[index]));
            textComponent.text = messages[index];

            if (autoSkip)
            {
                yield return new WaitForSeconds(3);
                bubble_animator.SetBool("IsWriting", false);
                textComponent.text = string.Empty;
                index++;
                continue;
            }
            else
            {
                bubble_animator.SetBool("IsWriting", false);

                while (!writingFinish)
                {
                    if (myController.Jump().GetKey())
                    {
                        writingFinish = true;
                        index++;
                        continue;
                    }

                    yield return new WaitForEndOfFrame();
                }


            }
        }

        bubble_animator.SetBool("IsShowing", false);
        yield return new WaitForSeconds(1);
        OnFinishRead.Invoke();
        textComponent.text = string.Empty;
    }

    private IEnumerator WriteDialogue(string message)
    {
        writingFinish = false;

        textComponent.text = string.Empty;

        bubble_animator.SetBool("IsShowing", true);
        bubble_animator.SetBool("IsWriting", true);

        yield return new WaitForSeconds(1);

        yield return StartCoroutine(TypeText(message));

        textComponent.text = message;

        if (autoSkip)
        {
            yield return new WaitForSeconds(3);
            bubble_animator.SetBool("IsShowing", false);
            bubble_animator.SetBool("IsWriting", false);
            OnFinishRead.Invoke();
        }
        else
        {
            bubble_animator.SetBool("IsWriting", false);

            while (!writingFinish)
            {
                if (myController.Jump().GetKey())
                {
                    writingFinish = true;
                }

                yield return new WaitForEndOfFrame();
            }

            bubble_animator.SetBool("IsShowing", false);

            OnFinishRead.Invoke();
        }

        textComponent.text = string.Empty;
    }

    private IEnumerator TypeText(string message)
    {
        foreach (char letter in message.ToCharArray())
        {
            textComponent.text += letter;

            if (speed <= 0)
                speed = 0.01f;

            typeSound.Play();

            yield return new WaitForSeconds(Time.deltaTime * (1/speed));
        }
    }
    
}
