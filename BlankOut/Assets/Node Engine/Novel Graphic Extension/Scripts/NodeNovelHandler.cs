﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeNovelHandler : MonoBehaviour {

    private static NodeNovelHandler _singleton;

    public static NodeNovelHandler Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = new GameObject("NodeNovelHandler", typeof(NodeNovelHandler)).GetComponent<NodeNovelHandler>();

            return _singleton;
        }
    }
}
