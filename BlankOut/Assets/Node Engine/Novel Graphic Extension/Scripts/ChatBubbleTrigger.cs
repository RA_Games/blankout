﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ChatBubbleTrigger : MonoBehaviour {

    public Actor actor;
    public LanguageValue language;
    public DialogueSheet dialogueSheet;
    public int index;

    public UnityEvent onDialogueFinish;

    public void ReadDialogue()
    {
        ChatBubble.GetBubbleInScene(actor).Write(dialogueSheet.Get(index).GetDialogue(language.value), FinishReading);
    }

    private void FinishReading()
    {
        onDialogueFinish.Invoke();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ChatBubbleTrigger))]
public class ChatBubbleTriggerEditor : Editor
{
    ChatBubbleTrigger myTarget;

    private void OnEnable()
    {
        myTarget = (ChatBubbleTrigger)target;
    }

    public override void OnInspectorGUI()
    {
        this.serializedObject.Update();
        myTarget.actor = (Actor)EditorGUILayout.ObjectField("Actor", myTarget.actor, typeof(Actor), false);
        myTarget.language = (LanguageValue)EditorGUILayout.ObjectField("Language", myTarget.language, typeof(LanguageValue), false);
        myTarget.dialogueSheet = (DialogueSheet)EditorGUILayout.ObjectField("Dialogue Sheet", myTarget.dialogueSheet, typeof(DialogueSheet), false);

        EditorGUILayout.PropertyField(this.serializedObject.FindProperty("onDialogueFinish"), true);

        if (myTarget.dialogueSheet != null && myTarget.language != null)
        {
            EditorGUILayout.Space();

            myTarget.index = EditorGUILayout.IntSlider("Index", myTarget.index, 0, myTarget.dialogueSheet.Count - 1);

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Preview: " + myTarget.dialogueSheet.Get(myTarget.index).GetDialogue(myTarget.language.value));
        }

        this.serializedObject.ApplyModifiedProperties();
    }
}
#endif
