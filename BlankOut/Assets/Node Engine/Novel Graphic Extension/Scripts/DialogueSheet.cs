﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Node Engine/Novel/Dialogue Sheet")]
public class DialogueSheet : ScriptableObject {

    [SerializeField]
    private List<Dialogue> dialogues = new List<Dialogue>();

    public int Count
    {
        get
        {
            return dialogues.Count;
        }
    }

    public void Add(Dialogue d)
    {
        dialogues.Add(d);
    }

    public void Remove(Dialogue d)
    {
        dialogues.Remove(d);
    }

    public Dialogue Get(int index)
    {
        return dialogues[index];
    }

    public Dialogue Get(string ID)
    {
        for (int i = 0; i < dialogues.Count; i++)
        {
            if (dialogues[i].ID == ID)
                return dialogues[i];
        }

        return null;
    }

    [System.Serializable]
    public class Dialogue
    {
        public string[] texts;

        [SerializeField]
        private string _id;

        public string ID
        {
            get
            {
                if(_id == null || _id == string.Empty)
                    _id = "#" + Random.Range(0, 9) + Random.Range(0, 9) + Random.Range(0, 9) + Random.Range(0, 9);

                return _id;
            }
        }

        public Dialogue()
        {
            texts = new string[System.Enum.GetValues(typeof(Languages)).Length];
        }

        public string GetDialogue(Languages language)
        {
            return texts[(int)language];
        }

        public void SetDialogue(Languages language, string text)
        {
            this.texts[(int)language] = text;
        }
    }
}

public enum Languages
{
    SPANISH, ENGLISH, RUSSIAN, GERMAN, PROTUGUESE, FRENCH, CHINESE
}
