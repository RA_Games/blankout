﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DialogueSheet))]
public class DialogueSheetEditor : Editor {

    DialogueSheet myTarget;
    Vector2 scrollPos;

    List<Languages> preview;
    GUIStyle myTextAreaStyle;

    private void OnEnable()
    {
        myTarget = (DialogueSheet)target;
        preview = new List<Languages>();
        for (int i = 0; i < myTarget.Count; i++)
            preview.Add(Languages.ENGLISH);

        EditorStyles.textArea.wordWrap = true;
        myTextAreaStyle = new GUIStyle(EditorStyles.textArea);
    }

    public override void OnInspectorGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        for (int i = 0; i < myTarget.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("ID: " + myTarget.Get(i).ID, GUILayout.Width(80));

            preview[i] = (Languages)EditorGUILayout.EnumPopup(preview[i]);

            if (GUILayout.Button("Remove"))
            {
                preview.Remove(preview[i]);
                myTarget.Remove(myTarget.Get(i));
                return;
            }

            EditorGUILayout.EndHorizontal();

            string currentContent = EditorGUILayout.TextArea(myTarget.Get(i).GetDialogue(preview[i]), myTextAreaStyle, GUILayout.Height(50));
            if (currentContent == string.Empty || currentContent == null || currentContent == "")
            {
                currentContent = "## Unsuported language of dialogue: " + myTarget.Get(i).ID + " in " + myTarget.name + " ##";
            }
            myTarget.Get(i).SetDialogue(preview[i], currentContent);
        }

        if (GUILayout.Button("Add Dialogue"))
        {
            myTarget.Add(new DialogueSheet.Dialogue());
            preview.Add(Languages.ENGLISH);
        }

        EditorGUILayout.EndScrollView();

        if (GUI.changed)
            EditorUtility.SetDirty(myTarget);
    }
}
