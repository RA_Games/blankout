﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(Text))]
public class MultiLanguageText : MonoBehaviour {

    public LanguageValue language;
    public DialogueSheet sheet;
    public int sheet_index;

    private Text _text_component;

    private Text TextComponent
    {
        get
        {
            if(_text_component == null)
                _text_component = GetComponent<Text>();

            return _text_component;
        }
    }

    private void Start()
    {
        UpdateValue();
    }

    public void UpdateValue()
    {
        if (language != null)
        {
            if (sheet != null)
            {
                TextComponent.text = sheet.Get(sheet_index).GetDialogue(language.value);
            }
            else
            {
                TextComponent.text = "Assign one DialogueSheet!";
            }
        }
        else
        {
            TextComponent.text = "Assign one language!";
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(MultiLanguageText))]
public class MultiLanguageTextEditor : Editor
{
    MultiLanguageText myTarget;

    private void OnEnable()
    {
        myTarget = (MultiLanguageText)target;
    }

    public override void OnInspectorGUI()
    {
        myTarget.language = (LanguageValue)EditorGUILayout.ObjectField("Language Value", myTarget.language, typeof(LanguageValue), false);
        myTarget.sheet = (DialogueSheet)EditorGUILayout.ObjectField("Dialogue Sheet", myTarget.sheet, typeof(DialogueSheet), false);

        if (myTarget.sheet != null)
        {
            myTarget.sheet_index = EditorGUILayout.IntSlider("Index", myTarget.sheet_index, 0, myTarget.sheet.Count-1);
        }

        if (GUI.changed)
        {
            myTarget.UpdateValue();
        }
    }
}
#endif
