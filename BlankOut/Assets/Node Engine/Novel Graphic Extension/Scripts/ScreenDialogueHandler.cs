﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ScreenDialogueHandler : MonoBehaviour
{
    public bool typeMode;
    public float letterPause = 0.2f;
    public bool activeTypeSound;
    public AudioClip typeClip;
    [SerializeField]
    private AudioSource typeSound;
    public bool autoSkip;
    [Space]

    [SerializeField]
    private Text text_area;
    [SerializeField]
    private Animator animator;

    private string lastText;
    private bool isDisplayed;
    private bool isTyping;

    private IEnumerator typingTextCorutine;
    private IEnumerator writeCorutine;
    private Action OnFinishWrite;

    public void Write(string text)
    {
        if(writeCorutine != null)
            StopCoroutine(writeCorutine);
        writeCorutine = WriteDialogue("",text);
        StartCoroutine(writeCorutine);
    }

    public void Write(string header, string text)
    {
        if (writeCorutine != null)
            StopCoroutine(writeCorutine);
        writeCorutine = WriteDialogue(header, text);
        StartCoroutine(writeCorutine);
    }

    public void Write(string header, string text, Action FinishRead)
    {
        OnFinishWrite = FinishRead;
        FinishRead();
    }

    public void Skip()
    {
        if (isTyping)
        {
            if (typingTextCorutine != null)
                StopCoroutine(typingTextCorutine);
            isTyping = false;
        }
        else
        {
            if (writeCorutine != null)
                StopCoroutine(writeCorutine);
            isDisplayed = false;
            animator.SetBool("Is Displayed", isDisplayed);
            if(OnFinishWrite != null)
                OnFinishWrite();
        }
    }

    private IEnumerator WriteDialogue(string header, string text)
    {
        text_area.text = string.Empty;
        isDisplayed = true;

        animator.SetBool("Is Displayed", isDisplayed);
        typingTextCorutine = TypeText(text);

        yield return new WaitForSeconds(1);

        if (typeMode)
        {
            StartCoroutine(typingTextCorutine);
            yield return new WaitUntil(() => !isTyping);
        } 

        text_area.text = text;

        if (autoSkip)
        {
            yield return new WaitForSeconds(3);
            isDisplayed = false;
            animator.SetBool("Is Displayed", isDisplayed);
        }
    }

    private IEnumerator TypeText(string message)
    { 
        lastText = message;
        isTyping = true;

        foreach (char letter in message.ToCharArray())
        {
            if (!isTyping)
                yield break;
            text_area.text += letter;
            if (activeTypeSound)
                typeSound.PlayOneShot(typeClip);
            yield return 0;
            yield return new WaitForSeconds(letterPause);
        }

        isTyping = false;
    }
}
