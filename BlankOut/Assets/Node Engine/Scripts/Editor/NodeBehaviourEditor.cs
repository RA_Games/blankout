﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(NodeBehaviour))]
public class NodeBehaviourEditor : Editor
{
    NodeBehaviour myTarget;
    Node selectedNode;
    string nodeiD;

    int dialogueIndex;
    bool defaultNode;
    bool lastDefaultNode;

    private void OnEnable()
    {
        myTarget = (NodeBehaviour)target;
        selectedNode = null;
    }

    public override void OnInspectorGUI()
    {
        foreach (Node node in myTarget.nodes)
        {
            if (node.isSelected && nodeiD != node.nodeId)
            {
                selectedNode = node;
                nodeiD = selectedNode.nodeId;

                if (selectedNode is NodeDialogue)
                {
                    var cast = (NodeDialogue)selectedNode;
                    try
                    {
                        for (int i = 0; i < cast.dialogueSheet.Count; i++)
                        {
                            if (cast.dialogueSheet.Get(i).ID == cast.dialogueID)
                            {
                                dialogueIndex = i;
                                break;
                            }
                        }
                    }
                    catch { }
                }

                break;
            }
        }

        if (selectedNode == null)
            return;

        EditorGUILayout.LabelField("Type", selectedNode.GetType().ToString());

        nodeiD = EditorGUILayout.TextField("Name", nodeiD);

        if (myTarget.defaultNode == null || myTarget.defaultNode == string.Empty || myTarget.defaultNode == "")
        {
            myTarget.defaultNode = selectedNode.nodeId;
        }

        defaultNode = (myTarget.defaultNode == selectedNode.nodeId);
        lastDefaultNode = defaultNode;

        defaultNode = EditorGUILayout.Toggle("Start Node", defaultNode);

        if (lastDefaultNode != defaultNode)
        {
            myTarget.defaultNode = selectedNode.nodeId;
        }

        if (nodeiD != selectedNode.nodeId)
        {
            selectedNode.nodeId = RenameNode(selectedNode, nodeiD);
        }

        if (selectedNode is NodeTriggerEvent)
        {
            var cast = (NodeTriggerEvent)selectedNode;

            cast.triggerEvent = (GameEvent)EditorGUILayout.ObjectField("Trigger Event", cast.triggerEvent, typeof(GameEvent), false);
        }

        if (selectedNode is NodeListenGameEvent)
        {
            var cast = (NodeListenGameEvent)selectedNode;

            cast.listenEvent = (GameEvent)EditorGUILayout.ObjectField("Listen Event", cast.listenEvent, typeof(GameEvent), false);
        }

        if (selectedNode is NodeWaitForSeconds)
        {
            var cast = (NodeWaitForSeconds)selectedNode;

            cast.seconds = EditorGUILayout.FloatField("Seconds", cast.seconds);
        }

        if (selectedNode is NodeDialogue)
        {
            var cast = (NodeDialogue)selectedNode;

            cast.dialogueType = (NodeDialogue.DialogueType)EditorGUILayout.EnumPopup("Dialogue Type", cast.dialogueType);

            if (cast.dialogueType == NodeDialogue.DialogueType.SCREEN)
            {
                cast.header = EditorGUILayout.TextField("Header", cast.header);
            }
            else
            {
                cast.actor = (Actor)EditorGUILayout.ObjectField("Actor", cast.actor, typeof(Actor), false);
            }

            cast.useDialogueSheet = EditorGUILayout.Toggle("Use Dialogue Sheet", cast.useDialogueSheet);

            if (cast.useDialogueSheet)
            {
                cast.language = (LanguageValue)EditorGUILayout.ObjectField("Language Value", cast.language, typeof(LanguageValue), false);

                cast.dialogueSheet = (DialogueSheet)EditorGUILayout.ObjectField("DialogueSheet", cast.dialogueSheet, typeof(DialogueSheet), false);

                if (cast.dialogueSheet != null)
                {
                    cast.useRangue = EditorGUILayout.Toggle("Use Rangue", cast.useRangue);

                    if (cast.useRangue)
                    {
                        EditorGUILayout.MinMaxSlider("Rangue", ref cast.rangueMinValue, ref cast.rangueMaxValue, 0, cast.dialogueSheet.Count - 1);
                        cast.rangueMinValue = (int)cast.rangueMinValue;
                        cast.rangueMaxValue = (int)cast.rangueMaxValue;

                        cast.previewLanguage = (Languages)EditorGUILayout.EnumPopup(cast.previewLanguage);
                        EditorGUILayout.Separator();
                        EditorGUILayout.LabelField(cast.dialogueSheet.Get((int)cast.rangueMinValue).ID + " - Start: ");
                        EditorGUILayout.LabelField(cast.dialogueSheet.Get((int)cast.rangueMinValue).GetDialogue(cast.previewLanguage));
                        EditorGUILayout.Separator();
                        EditorGUILayout.LabelField(cast.dialogueSheet.Get((int)cast.rangueMaxValue).ID + " - End: ");
                        EditorGUILayout.LabelField(cast.dialogueSheet.Get((int)cast.rangueMaxValue).GetDialogue(cast.previewLanguage));
                    }
                    else
                    {
                        dialogueIndex = EditorGUILayout.IntSlider("Dialogue ID", dialogueIndex, 0, cast.dialogueSheet.Count - 1);

                        cast.dialogueID = cast.dialogueSheet.Get(dialogueIndex).ID;
                        EditorGUILayout.LabelField(cast.dialogueID);
                        cast.previewLanguage = (Languages)EditorGUILayout.EnumPopup(cast.previewLanguage);
                        EditorGUILayout.LabelField("Preview: ");
                        EditorGUILayout.LabelField(cast.dialogueSheet.Get(cast.dialogueID).GetDialogue(cast.previewLanguage));
                    }
                }

            }
            else
            {
                cast.text = EditorGUILayout.TextField("Text", cast.text);
            }
        }

        if (selectedNode is NodeEnd)
        {
            var cast = (NodeEnd)selectedNode;

            cast.exitType = (NodeEnd.ExitType)EditorGUILayout.EnumPopup("Exit Type", cast.exitType);

            switch (cast.exitType)
            {
                case NodeEnd.ExitType.LOAD_SCENE:
                    cast.nextScene = EditorGUILayout.TextField("Next Scene", cast.nextScene);
                    cast.asynLoad = EditorGUILayout.Toggle("Async Load", cast.asynLoad);
                    cast.loadMode = (LoadSceneMode) EditorGUILayout.EnumPopup("Load Scene Mode", cast.loadMode);
                    break;
                case NodeEnd.ExitType.OPEN_BEHAVIOUR:
                    cast.nextNodeBehaviour = (NodeBehaviour)EditorGUILayout.ObjectField("Next Nodebehaviour", cast.nextNodeBehaviour, typeof(NodeBehaviour), false);
                    break;
                case NodeEnd.ExitType.NONE:
                    break;
            }
        }

        if (selectedNode is NodeAddReader)
        {
            var cast = (NodeAddReader)selectedNode;

            cast.nextNodeBehaviour = (NodeBehaviour)EditorGUILayout.ObjectField("Next Nodebehaviour", cast.nextNodeBehaviour, typeof(NodeBehaviour), false);
        }

        EditorUtility.SetDirty(myTarget);

    }

    private string RenameNode(Node newNode, string newName)
    {
        bool isFound = false;
        string nodeId = newName;

        foreach (Node node in myTarget.nodes)
        {
            if (nodeId == node.nodeId)
            {
                isFound = true;
                break;
            }
        }

        if (!isFound)
            return newName;

        isFound = false;

        for (int i = 0; i < 500; i++)
        {
            isFound = false;
            nodeId = newNode.nodeId + " " + i;

            foreach (Node node in myTarget.nodes)
            {
                if (nodeId == node.nodeId)
                {
                    isFound = true;
                    break;
                }
            }

            if (!isFound)
                return nodeId;
        }

        return nodeId;
    }
}