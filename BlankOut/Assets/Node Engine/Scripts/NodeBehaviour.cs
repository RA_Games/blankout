﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[CreateAssetMenu(menuName = "Node Engine/Node Page/Create")]
public class NodeBehaviour : ScriptableObject, ISerializationCallbackReceiver
{
    public List<Node> nodes = new List<Node>();
    [SerializeField]
    private List<NodeTriggerEvent> triggerNodes = new List<NodeTriggerEvent>();
    [SerializeField]
    private List<NodeListenGameEvent> listenNodes = new List<NodeListenGameEvent>();
    [SerializeField]
    private List<NodeWaitForSeconds> waitNodes = new List<NodeWaitForSeconds>();
    [SerializeField]
    private List<NodeEnd> nodeEnds = new List<NodeEnd>();
    [SerializeField]
    private List<NodeAddReader> nodeReaders = new List<NodeAddReader>();
    [SerializeField]
    private List<NodeDialogue> nodeDialogues = new List<NodeDialogue>();

    public string defaultNode;

    public void OnAfterDeserialize()
    {
        nodes.Clear();

        MergeLists(nodes, triggerNodes);
        MergeLists(nodes, listenNodes);
        MergeLists(nodes, waitNodes);
        MergeLists(nodes, nodeEnds);
        MergeLists(nodes, nodeReaders);
        MergeLists(nodes, nodeDialogues);
    }

    public Node GetStartNode()
    {
        foreach (Node node in nodes)
        {
            if (node.nodeId == defaultNode)
            {
                return node;
            }
        }

        return null;
    }

    public void OnBeforeSerialize()
    {
        triggerNodes.Clear();
        listenNodes.Clear();
        waitNodes.Clear();
        nodeEnds.Clear();
        nodeReaders.Clear();
        nodeDialogues.Clear();

        foreach (Node node in nodes)
        {
            if (node is NodeListenGameEvent)
            {
                listenNodes.Add(node as NodeListenGameEvent);
            }

            if (node is NodeTriggerEvent)
            {
                triggerNodes.Add(node as NodeTriggerEvent);
            }

            if (node is NodeWaitForSeconds)
            {
                waitNodes.Add(node as NodeWaitForSeconds);
            }

            if (node is NodeEnd)
            {
                nodeEnds.Add(node as NodeEnd);
            }

            if (node is NodeAddReader)
            {
                nodeReaders.Add(node as NodeAddReader);
            }

            if (node is NodeDialogue)
            {
                nodeDialogues.Add(node as NodeDialogue);
            }
        }
    }

    private void MergeLists<T>(List<Node> first, List<T> another)
    {
        foreach (T t in another)
        {
            first.Add(t as Node);
        }
    }
}