﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum ConnectionPointType { In, Out }

[System.Serializable]
public class ConnectionPoint
{
    public Rect rect;

    public ConnectionPointType type;
    public Node node;
#if UNITY_EDITOR
    public GUIStyle style;
#endif
    public Action<ConnectionPoint> OnClickConnectionPoint;

#if UNITY_EDITOR
    public ConnectionPoint(Node node, ConnectionPointType type, GUIStyle style, Action<ConnectionPoint> OnClickConnectionPoint)
    {
        this.type = type;
        this.style = style;
        this.node = node;
        this.OnClickConnectionPoint = OnClickConnectionPoint;
        rect = new Rect(0, 0, 10f, 20f);
    }
#endif

    public ConnectionPoint()
    {

    }

#if UNITY_EDITOR
    public void Draw()
    {
        if (node == null)
            return;

        rect.y = node.rect.y + (node.rect.height * 0.5f) - rect.height * 0.5f;

        switch (type)
        {
            case ConnectionPointType.In:
                rect.x = node.rect.x - rect.width + 8f;
                break;

            case ConnectionPointType.Out:
                rect.x = node.rect.x + node.rect.width - 8f;
                break;
        }

        if (GUI.Button(rect, "", style))
        {
            if (OnClickConnectionPoint != null)
            {
                OnClickConnectionPoint(this);
            }
        }
    }
#endif
}