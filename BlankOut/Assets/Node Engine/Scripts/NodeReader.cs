﻿using UnityEngine;
using UnityEngine.Events;

public class NodeReader : MonoBehaviour {

    public NodeBehaviour nodeBehaviour;
    public bool startOnAwake = true;
    private Node current;

    IntegerValue value;
    DoubleValue Dvalue;

    bool isActive = false;

    public UnityEvent onStopReading;

    private void Start()
    {
        if (startOnAwake)
        {
            StartRead();
        }
    }

    private void Update()
    {
        if (current != null)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                StopReading();
            }
        }
    }

    public void SkipNode()
    {
        current.CancelRead();
        Continue();
    }

    public void StartRead()
    {
        isActive = true;
        current = nodeBehaviour.GetStartNode();
        current.ReadNode(Continue);
    }

    public void StopReading()
    {
        isActive = false;
        onStopReading.Invoke();
    }

    private void Continue()
    {
        current = current.nextNode.GetNode(nodeBehaviour);

        if (current != null)
        {
            if (isActive)
            {
                current.ReadNode(Continue);
            }
        }
    }
}
