﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Node
{
    public Rect rect;
    [SerializeField]
    public string nodeId = "Node";
    public string title;
    public bool isDragged;
    public bool isSelected;

    public NextNode nextNode;
    private Action _OnFinishNode;

    public List<Rect> content = new List<Rect>();

#if UNITY_EDITOR
    public GUIStyle style;
    public GUIStyle defaultNodeStyle;
    public GUIStyle selectedNodeStyle;

    public ConnectionPoint inPoint;
    public ConnectionPoint outPoint;
#endif

    public Action<Node> OnRemoveNode;

    [Serializable]
    public struct NextNode
    {
        [SerializeField]
        private string nextNodeId;

        public void SetNextNode(string id)
        {
            nextNodeId = id;
        }

        public void UnsetNextNode()
        {
            nextNodeId = string.Empty;
        }

        public Node GetNode(NodeBehaviour behaviour)
        {
            foreach (Node n in behaviour.nodes)
            {
                if (nextNodeId == n.nodeId)
                {
                    return n;
                }
            }

            return null;
        }
    }

#if UNITY_EDITOR
    public Node(Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Node> OnClickRemoveNode)
    {
        rect = new Rect(position.x, position.y, width, height);
        OnRemoveNode = OnClickRemoveNode;
        inPoint = new ConnectionPoint(this,ConnectionPointType.In, inPointStyle, OnClickInPoint);
        outPoint = new ConnectionPoint(this, ConnectionPointType.Out, outPointStyle, OnClickOutPoint);

        defaultNodeStyle = nodeStyle;
        selectedNodeStyle = selectedStyle;
        style = nodeStyle;
    }
#endif

    public Node()
    {

    }

#if UNITY_EDITOR

    public void Rebuild(Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Node> OnClickRemoveNode)
    {
        inPoint.node = this;
        outPoint.node = this;
        OnRemoveNode = OnClickRemoveNode;
        inPoint.OnClickConnectionPoint = OnClickInPoint;
        outPoint.OnClickConnectionPoint = OnClickOutPoint;
    }

    public void Drag(Vector2 delta)
    {
        rect.position += delta;
    }

    public void Draw()
    {
        inPoint.Draw();
        outPoint.Draw();

        GUI.Box(rect, title, style);

        var centeredStyle = GUI.skin.GetStyle("Label");
        centeredStyle.alignment = TextAnchor.UpperCenter;

        Rect test = rect;
        test.y += (rect.height / 2) - 10;

        GUI.Label(test, nodeId, centeredStyle);

        content.Clear();
    }

    public bool ProcessEvents(Event e)
    {
        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.button == 0)
                {
                    if (rect.Contains(e.mousePosition))
                    {
                        isDragged = true;
                        GUI.changed = true;
                        isSelected = true;
                        style = selectedNodeStyle;
                    }
                    else
                    {
                        GUI.changed = true;
                        isSelected = false;
                        style = defaultNodeStyle;
                    }
                }

                break;

            case EventType.MouseUp:
                isDragged = false;
                break;

            case EventType.MouseDrag:
                if (e.button == 0 && isDragged)
                {
                    Drag(e.delta);
                    e.Use();
                    return true;
                }
                break;
        }

        return false;
    }
#endif
    public void OnClickRemoveNode()
    {
        if (OnRemoveNode != null)
        {
            OnRemoveNode(this);
        }
    }

    public void ReadNode(Action onFinishNode)
    {
        OnStartRead();
        _OnFinishNode = onFinishNode;
        Read();
    }

    public void FinishRead()
    {
        OnFinishRead();
        _OnFinishNode.Invoke();
    }

    protected abstract void Read();
    public abstract void CancelRead();

    protected virtual void OnStartRead() { }
    protected virtual void OnFinishRead() { }
    public virtual void OnEventRaise() { }
}