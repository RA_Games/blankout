﻿using System;
using UnityEngine;

[Serializable]
public class NodeListenGameEvent : Node
{
    public GameEvent listenEvent;
#if UNITY_EDITOR
    public NodeListenGameEvent(Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Node> OnClickRemoveNode) : base(position, width, height, nodeStyle, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode)
    {
    }
#endif
    protected override void Read()
    {
        listenEvent.Add(this);
    }

    public override void OnEventRaise()
    {
        listenEvent.Remove(this);
        FinishRead();
    }

    public override void CancelRead()
    {
        listenEvent.Remove(this);
    }
}
