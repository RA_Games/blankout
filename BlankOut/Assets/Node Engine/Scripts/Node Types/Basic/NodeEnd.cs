﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

[Serializable]
public class NodeEnd : Node
{
    public enum ExitType
    {
        LOAD_SCENE, OPEN_BEHAVIOUR, NONE
    }

    public ExitType exitType;

    //OPEN_BEHAVIOUR
    public NodeBehaviour nextNodeBehaviour;

    //LOAD_SCENE
    public string nextScene;
    public bool asynLoad;
    public LoadSceneMode loadMode;

#if UNITY_EDITOR
    public NodeEnd(Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Node> OnClickRemoveNode) : base(position, width, height, nodeStyle, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode)
    {
    }
#endif

    public NodeEnd()
    {

    }


    protected override void Read()
    {
        switch (exitType)
        {
            case ExitType.LOAD_SCENE:
                if (asynLoad)
                {
                    NodeHandler.Singleton.StartCoroutine(LoadSceneAsync());
                }
                else
                {
                    SceneManager.LoadScene(nextScene, loadMode);
                }
                break;
            case ExitType.OPEN_BEHAVIOUR:
                NodeReader reader = new GameObject("Node Reader", typeof(NodeReader)).GetComponent<NodeReader>();
                reader.nodeBehaviour = nextNodeBehaviour;
                FinishRead();
                break;
            case ExitType.NONE:
                FinishRead();
                break;
        }
    }

    private IEnumerator LoadSceneAsync()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(nextScene, loadMode);

        while (!async.isDone)
        {
            yield return new WaitForEndOfFrame();
        }

        FinishRead();

        yield break;
    }

    public override void CancelRead()
    {
       
    }
}
