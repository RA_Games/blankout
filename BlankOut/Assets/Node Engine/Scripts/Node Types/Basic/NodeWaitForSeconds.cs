﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class NodeWaitForSeconds : Node
{
    public float seconds;
#if UNITY_EDITOR
    public NodeWaitForSeconds(Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Node> OnClickRemoveNode) : base(position, width, height, nodeStyle, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode)
    {
    }
#endif
    public override void CancelRead()
    {
        NodeHandler.Singleton.StopAllCoroutines();
    }

    protected override void Read()
    {
        NodeHandler.Singleton.StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(seconds);

        FinishRead();
    }
}
