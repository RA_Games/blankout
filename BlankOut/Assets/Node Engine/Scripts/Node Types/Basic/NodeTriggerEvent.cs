﻿using UnityEngine;
using System;

[Serializable]
public class NodeTriggerEvent : Node
{
    public GameEvent triggerEvent;

#if UNITY_EDITOR
    public NodeTriggerEvent(Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Node> OnClickRemoveNode) : base(position, width, height, nodeStyle, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode)
    {
    }
#endif

    public NodeTriggerEvent()
    {

    }

    public override void CancelRead()
    {
        
    }

    protected override void Read()
    {
        triggerEvent.Raise();
        FinishRead();
    }
}
