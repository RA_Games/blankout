﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu(menuName = "Node Engine/Dependency Inyector/Novel Graphic")]
public class NovelGraphicDependency : Dependencie
{
    public ScreenDialogueHandler dialogueHandler;
    private ScreenDialogueHandler _instance;

    public override T GetComponent<T>()
    {
        if(dialogueHandler is T)
        {
            if (_instance == null)
                _instance = Instantiate(dialogueHandler);

            return (T)Convert.ChangeType(_instance, typeof(ScreenDialogueHandler)); 
        }

        return (T)Convert.ChangeType(null, typeof(MonoBehaviour));
    }
}
