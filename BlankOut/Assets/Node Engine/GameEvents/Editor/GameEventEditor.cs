﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameEvent))]
public class GameEventEditor : Editor
{
    private GameEvent myTarget;

    public void OnEnable()
    {
        myTarget = (GameEvent)target;
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button(myTarget.name))
        {
            myTarget.Raise();
        }
    }
}