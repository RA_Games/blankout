﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Node Engine/Game Event/Create")]
public class GameEvent : ScriptableObject
{
    public List<GameEventListener> listeners = new List<GameEventListener>();
    public List<Node> nodes = new List<Node>();

    public void Add(GameEventListener listener)
    {
        if (!listeners.Contains(listener))
            listeners.Add(listener);
    }

    public void Add(Node node)
    {
        if (!nodes.Contains(node))
            nodes.Add(node);
    }

    public void Remove(GameEventListener listener)
    {
        listeners.Remove(listener);
    }

    public void Remove(Node node)
    {
        nodes.Remove(node);
    }

    public void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnRaiseEvent();
        }

        for (int i = nodes.Count - 1; i >= 0; i--)
        {
            nodes[i].OnEventRaise();
        }
    }
}
