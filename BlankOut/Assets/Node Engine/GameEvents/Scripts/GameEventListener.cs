﻿using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    public GameEvent listenEvent;
    public UnityEvent response;

    private void OnEnable()
    {
        listenEvent.Add(this);
    }

    private void OnDisable()
    {
        listenEvent.Remove(this);
    }

    public void OnRaiseEvent()
    {
        response.Invoke();
    }
}
