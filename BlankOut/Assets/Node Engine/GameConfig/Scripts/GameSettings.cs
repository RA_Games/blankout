﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Node Engine/Game Settings/Create")]
public class GameSettings : ScriptableObject {

    public Languages language;
}
