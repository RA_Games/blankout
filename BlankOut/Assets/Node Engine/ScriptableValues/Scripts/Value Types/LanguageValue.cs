﻿using UnityEngine;

[CreateAssetMenu(menuName = "Node Engine/Values/Language")]
public class LanguageValue : ScriptableValue<Languages>
{
    public override int CompareTo(Languages other)
    {
        if (other == value)
            return 1;
        else
            return -1;
    }

    public override int CompareTo(object obj)
    {
        if (obj == GetType())
            return 1;
        else
            return -1;
    }

    public override bool Equals(Languages other)
    {
        return (value == other);
    }
}