﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Project Sofia/My Control")]
public class MyController : ScriptableController
{
    [SerializeField]
    private ControllerType mode = ControllerType.PC;

    [SerializeField]
    private CrossPlatformAxis horizontal;
    [SerializeField]
    private CrossPlatformAxis vertical;

    [SerializeField]
    private CrossPlatformKeys jump;

    [SerializeField]
    private CrossPlatformKeys start;

    [SerializeField]
    private CrossPlatformKeys action;

    private ScriptableAxis horizontal_axis;
    private ScriptableAxis vertical_axis;
    private ScriptableKey jump_key;
    private ScriptableKey start_key;
    private ScriptableKey action_key;

    public void SetMode(ControllerType mode)
    {
        this.mode = mode;

        horizontal_axis = null;
        vertical_axis = null;
        jump_key = null;
        start_key = null;
        action_key = null;
        //And so..
    }

    public void OnValidate()
    {
        SetMode(mode);
    }

    public Vector2 GetAxis()
    {
        if (horizontal_axis == null)
            horizontal_axis = horizontal.Get(mode);

        if (vertical_axis == null)
            vertical_axis = vertical.Get(mode);

        return new Vector3(horizontal_axis.GetAxis(), vertical_axis.GetAxis());
    }

    public Vector2 GetAxisRaw()
    {
        if (horizontal_axis == null)
            horizontal_axis = horizontal.Get(mode);

        if (vertical_axis == null)
            vertical_axis = vertical.Get(mode);

        return new Vector3(horizontal_axis.GetAxisRaw(), vertical_axis.GetAxisRaw());
    }

    public ScriptableAxis GetHorizontalAxis()
    {
        if (horizontal_axis == null)
            horizontal_axis = horizontal.Get(mode);

        return horizontal_axis;
    }

    public ScriptableAxis GetVerticalAxis()
    {
        if (vertical_axis == null)
            vertical_axis = vertical.Get(mode);

        return vertical_axis;
    }

    public ScriptableKey Jump()
    {
        if (jump_key == null)
            jump_key = jump.GetKey(mode);

        return jump_key;
    }

    public ScriptableKey Action()
    {
        if (action_key == null)
            action_key = action.GetKey(mode);

        return action_key;
    }

    public ScriptableKey Start()
    {
        if (start_key == null)
            start_key = start.GetKey(mode);

        return start_key;
    }
}
