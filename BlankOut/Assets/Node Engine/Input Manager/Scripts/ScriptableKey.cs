﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Node Engine/Input Manager/Scriptable Key")]
public class ScriptableKey : ScriptableInput {

    public PcButtons pc_butons;

    public XboxButtons xbox_buttons;

    public PlaystationButtons play_buttons;

    public SwitchButtons switch_buttons;

    public MouseButtons mouse_buttons;

    public KeyCode keyCode;

    public bool GetKey()
    {
        if (controllerType == ControllerType.PC)
        {
            return Input.GetKey(keyCode);
        }
        else
        {
            return Input.GetButton(controllerName);
        }
    }

    public bool GetKeyDown()
    {
        if (controllerType == ControllerType.PC)
        {
            return Input.GetKeyDown(keyCode);
        }
        else
        {        
            return Input.GetButtonDown(controllerName);
        }
    }

    public bool GetKeyUp()
    {
        if (controllerType == ControllerType.PC)
        {
            return Input.GetKeyUp(keyCode);
        }
        else
        {
            return Input.GetButtonUp(controllerName);
        }
    }
}
