﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Node Engine/Input Manager/Scriptable Axis")]
public class ScriptableAxis : ScriptableInput
{

    public string inputName;

    private bool _isDown;

    public float GetAxis()
    {
        return Input.GetAxis(inputName);
    }

    public bool GetAxisDown()
    {
        if (Input.GetAxis(inputName) != 0.1f)
        {
            if (!_isDown)
            {
                _isDown = true;
                return true;
            }
        }
        else
        {
            _isDown = false;
        }

        return false;
    }

    public float GetAxisRaw()
    {
        return Input.GetAxisRaw(inputName);
    }
}
