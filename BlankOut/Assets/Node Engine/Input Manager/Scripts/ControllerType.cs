﻿public enum ControllerType
{
    PC, XBOX, PLAYSTATION, SWITCH
}

public enum MouseButtons
{
    LEFT, RIGHT, SCROLL
}

public enum PcButtons
{
    MOUSE, KEYBOARD
}

public enum XboxButtons
{
    A, B, X, Y, RB, LB, BACK, START, LEFT_STICK, RIGHT_STICK, VIEW, XBOX_BUTTON, MENU_BUTTON
}

public enum PlaystationButtons
{
    CIRCLE, TRIANGLE, X, SQUARE, SELECT, START, R1, L1, R3, L3
}

public enum SwitchButtons
{
    
}

public enum XboxAxis
{
    LEFT_STICK, RIGHT_STICK, D_PAD, LT, RT
}

public enum PlaystationAxis
{

}

public enum SwitchAxis
{

}

public enum PcAxis
{
    MOUSE, KEY
}