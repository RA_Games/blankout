﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScriptableInput : ScriptableObject {
    public Sprite icon;
    public AnimationSheet animationSheet;
    public ControllerType controllerType;
    public string controllerName;
}
