﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlDisplay : MonoBehaviour {

    [SerializeField]
    private MyController controller;

    public enum Input
    {
        HORIZONTAL, VERTICAL, SUBMIT
    }

    public bool isAnimated;

    public Input inputToDisplay;

    [SerializeField]
    private Image image_component;
    public bool startOnAwake;
    private bool isDisplay;

    private int animationIndex = 0;
    private AnimationSheet animationSheet;
    private float animationTime;

    private void Awake()
    {
        Hide();
    }

    private void Start()
    {
        if (startOnAwake)
        {
            Display();
        }
    }

    public void Display()
    {
        animationIndex = 0;
        isDisplay = true;

        if (isAnimated)
        {
            animationSheet = GetInput().animationSheet;
            animationTime = 0;
        }
        else
        {
            image_component.sprite = GetInput().icon;
            image_component.SetNativeSize();
        }

        image_component.enabled = true;
    }

    public void Update()
    {
        if (isDisplay && isAnimated)
        {
            if (Time.time > animationTime)
            {
                image_component.sprite = animationSheet.sprites[animationIndex++];

                if (animationIndex >= animationSheet.sprites.Length)
                    animationIndex = 0;

                image_component.SetNativeSize();

                animationTime = Time.time + animationSheet.speed;
            }
        }
    }

    private ScriptableInput GetInput()
    {
        switch (inputToDisplay)
        {
            case Input.HORIZONTAL:
                return controller.GetHorizontalAxis();
            case Input.VERTICAL:
                return controller.GetVerticalAxis();
            case Input.SUBMIT:
                return controller.Jump();
        }

        return null;
    }

    public void Hide()
    {
        image_component.enabled = false;
        isDisplay = false;
    }
}
