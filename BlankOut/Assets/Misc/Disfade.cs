﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disfade : MonoBehaviour {

    [SerializeField] Collider2D _coll;
    [SerializeField] SpriteRenderer _rend;

    [Range(0, 1)] public float startFadePercentage = 0.5f;

    private float fadeAmount;
    private float maxFade;

    Vector3 leftSide;
    Vector3 rightSide;

    Transform player;
    float maxDistance;

    float lastFadeamount = 1;

    void Start () {
        leftSide = _coll.bounds.center - (new Vector3(_coll.bounds.extents.x, 0,0));
        rightSide = _coll.bounds.center + (new Vector3(_coll.bounds.extents.x, 0, 0));

        maxDistance = Vector3.Distance(leftSide, rightSide);
        maxFade = 1 - startFadePercentage;
    }
	
	void Update () {

        if (player != null)
        {
            float currentDistance = Vector3.Distance(rightSide, player.position) / maxDistance;

            if (currentDistance <= startFadePercentage)
            {
                fadeAmount = currentDistance / maxFade;

                if (fadeAmount < lastFadeamount)
                {
                    Color fade = _rend.color;
                    fade.a = fadeAmount;
                    _rend.color = fade;

                    lastFadeamount = fadeAmount;
                }

                if (fadeAmount <= 0.2f)
                {
                    gameObject.SetActive(false);
                }
            }
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            player = collision.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            player = null;
        }
    }
}
