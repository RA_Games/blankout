﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation3D : MonoBehaviour {

    Vector3 rotation;
    float angularVelocity;
    float dampingVelocity;

    private void Start()
    {
        rotation = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));
        angularVelocity = Random.Range(1,5);
        dampingVelocity = Random.Range(0.3f,1f);
    }

    void Update ()
    {
        transform.Rotate(rotation*Time.deltaTime*angularVelocity);

        transform.position = new Vector3(transform.position.x, transform.position.y + ((Mathf.Cos(Time.time*dampingVelocity))*Time.deltaTime)/5, transform.position.z);
	}
}
