﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stairs : MonoBehaviour {

    [SerializeField] MyController controller;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (controller.GetVerticalAxis().GetAxisRaw() > 0)
            {
                collision.attachedRigidbody.velocity = new Vector2(0, 5);

            }
            else if (controller.GetVerticalAxis().GetAxisRaw() < 0)
            {
                collision.attachedRigidbody.velocity = new Vector2(0, -5);
            }
            else
            {
                collision.attachedRigidbody.velocity = new Vector2(0, 0);
            }
        }
    }
}
