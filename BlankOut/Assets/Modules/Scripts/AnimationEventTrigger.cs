﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEventTrigger : MonoBehaviour {

    public UnityEvent onAnimationStart;
    public UnityEvent onAnimationPoint;
    public UnityEvent onAnimationEnd;

    public void OnAnimationStart()
    {
        onAnimationStart.Invoke();
    }

    public void OnAnimationPoint()
    {
        onAnimationPoint.Invoke();
    }

    public void OnAnimationEnd()
    {
        onAnimationEnd.Invoke();
    }
}
