﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputEventTrigger : MonoBehaviour {

    public UnityEvent onPressedDown;
    public UnityEvent onPressed;

    [SerializeField] MyController controller;

    private enum Buttons
    {
        START, AXIS_RIGHT, AXIS_DOWN, AXIS_LEFT, AXIS_UP, ACTION, JUMP
    }

    [SerializeField] Buttons button;

    private void Update()
    {
        switch (button)
        {
            case Buttons.START:

                if (controller.Start().GetKeyDown())
                {
                    onPressedDown.Invoke();
                }

                if (controller.Start().GetKey())
                {
                    onPressed.Invoke();
                }
                break;
            case Buttons.AXIS_RIGHT:
                if (controller.GetAxis().x >= 0.1f)
                {
                    if (controller.GetHorizontalAxis().GetAxisDown())
                    {
                        onPressedDown.Invoke();
                    }

                    onPressed.Invoke();
                }
                break;
            case Buttons.AXIS_DOWN:
                if (controller.GetAxis().y <= -0.1f)
                {
                    onPressed.Invoke();
                }
                break;
            case Buttons.AXIS_LEFT:
                if (controller.GetAxis().x <= -0.1f)
                {
                    onPressed.Invoke();
                }
                break;
            case Buttons.AXIS_UP:
                if (controller.GetAxis().y >= 0.1f)
                {
                    onPressed.Invoke();
                }
                break;
            case Buttons.ACTION:
                if (controller.Action().GetKeyDown())
                {
                    onPressedDown.Invoke();
                }

                if (controller.Action().GetKey())
                {
                    onPressed.Invoke();
                }
                break;
            case Buttons.JUMP:
                if (controller.Jump().GetKeyDown())
                {
                    onPressedDown.Invoke();
                }

                if (controller.Jump().GetKey())
                {
                    onPressed.Invoke();
                }
                break;
        }
    }

}
