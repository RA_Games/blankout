﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFollower : MonoBehaviour {

    public Transform target;

    public Vector3 offset;

    public float speed;

	// Use this for initialization
	public void SetTarget (Transform target) {
        this.target = target;
	}

    public void SetOffsetX(float offsetX)
    {
        this.offset.x = offsetX;
    }

    public void SetOffsetY(float offsetY)
    {
        this.offset.y = offsetY;
    }

    public void SetOffsetZ(float offsetZ)
    {
        this.offset.z = offsetZ;
    }
    // Update is called once per frame
    void LateUpdate () {
        transform.position = Vector3.MoveTowards(transform.position, target.position + offset, Time.smoothDeltaTime * speed);
	}
}
