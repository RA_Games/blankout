﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Life : MonoBehaviour {

    public ValueBar valueBar;

    public UnityEvent onAddLife;
    public UnityEvent onSubstractLife;
    public UnityEvent onLifeChangue;
    public UnityEvent onKill;


    public void AddLife(int amount)
    {
        if (amount > 0)
        {
            valueBar.initValue += amount;
            valueBar.initValue = Mathf.Clamp(valueBar.initValue, 0, valueBar.maxValue);
            onAddLife.Invoke();
            onLifeChangue.Invoke();
        }
    }

    public void ResetLife()
    {
        valueBar.initValue = valueBar.maxValue;
        onLifeChangue.Invoke();
    }

    public void SetLife(int amount)
    {
        if (amount >= 0)
        {
            valueBar.initValue = amount;
            valueBar.initValue = Mathf.Clamp(valueBar.initValue, 0, valueBar.maxValue);
            onLifeChangue.Invoke();
        }
    }

    public void SubstractLife(int amount)
    {
        if (amount > 0)
        {
            valueBar.initValue -= amount;
            valueBar.initValue = Mathf.Clamp(valueBar.initValue, 0, valueBar.maxValue);
            onLifeChangue.Invoke();
            onSubstractLife.Invoke();

            if (valueBar.initValue == 0)
            {
                onKill.Invoke();
            }
        }
    }
}

[System.Serializable]
public class ValueBar
{
    [Range(0, 1000)]
    public int maxValue = 100;

    public int initValue;

    public float CurrentValue
    {
        get
        {
            return initValue / maxValue;
        }
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(ValueBar))]
public class ValueBarDrawer: PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        var rh = position.height / 3;

        var bar = new Rect(position.x, position.y, position.width, rh);
        var maxLabel = new Rect(position.x, position.y + rh, position.width / 2, rh);
        var maxRect = new Rect(position.x + position.width / 2, position.y + rh, position.width / 2, rh);
        var initRect = new Rect(position.x, position.y + rh * 2, position.width, rh);

        EditorGUI.ProgressBar(bar, (float)property.FindPropertyRelative("initValue").intValue / (float)property.FindPropertyRelative("maxValue").intValue, (((float)property.FindPropertyRelative("initValue").intValue / (float)property.FindPropertyRelative("maxValue").intValue)*100).ToString("F0") + "%");

        EditorGUI.LabelField(maxLabel, "Max Value");

        property.FindPropertyRelative("maxValue").intValue = EditorGUI.IntField(maxRect, property.FindPropertyRelative("maxValue").intValue);

        int max = property.FindPropertyRelative("maxValue").intValue;

        if (max < 0)
        {
            property.FindPropertyRelative("maxValue").intValue = 0;
        }

        EditorGUI.IntSlider(initRect, property.FindPropertyRelative("initValue"), 0, max);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 50.0f;
    }
}
#endif
