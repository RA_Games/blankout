﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(BoxCollider2D))]
public class TriggerEventZone : MonoBehaviour {

    public UnityEvent onEnter;
    public UnityEvent onExit;

    public bool whitelistEnabled;

    public bool blackListEnabled;

    public List<string> whiteList = new List<string>();
    public List<string> blackList = new List<string>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!whitelistEnabled && !blackListEnabled)
        {
            onEnter.Invoke();
            return;
        }

        if (whitelistEnabled)
        {
            foreach (string tag in whiteList)
            {
                if (collision.tag == tag)
                {
                    onEnter.Invoke();
                    return;
                }
            }

            return;
        }

        if (blackListEnabled)
        {
            foreach (string tag in blackList)
            {
                if (collision.tag == tag)
                {
                    onEnter.Invoke();
                    return;
                }
            }

            return;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (whitelistEnabled)
        {
            foreach (string tag in whiteList)
            {
                if (other.tag == tag)
                {
                    onExit.Invoke();
                    return;
                }
            }

            return;
        }

        if (blackListEnabled)
        {
            foreach (string tag in blackList)
            {
                if (other.tag == tag)
                {
                    onExit.Invoke();
                    return;
                }
            }

            return;
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(TriggerEventZone))]
public class TriggerEventZoneEditor: Editor
{
    TriggerEventZone myTarget;

    private void OnEnable()
    {
        myTarget = (TriggerEventZone)target;
    }

    public override void OnInspectorGUI()
    {
        TriggerEventZone toggle = (TriggerEventZone)target;

        SerializedProperty onEnter = serializedObject.FindProperty("onEnter");
        SerializedProperty onExit = serializedObject.FindProperty("onExit");

        EditorGUILayout.PropertyField(onEnter);
        EditorGUILayout.PropertyField(onExit);

        if (GUI.changed)
        {
            serializedObject.ApplyModifiedProperties();
        }

        if (!myTarget.whitelistEnabled && !myTarget.blackListEnabled)
        {
            myTarget.whitelistEnabled = EditorGUILayout.Toggle("Whitelist", myTarget.whitelistEnabled);
            myTarget.blackListEnabled = EditorGUILayout.Toggle("Blacklist", myTarget.blackListEnabled);
        }

        if (myTarget.whitelistEnabled)
        {
            myTarget.whitelistEnabled = EditorGUILayout.Toggle("Whitelist", myTarget.whitelistEnabled);

            for (int i = 0; i < myTarget.whiteList.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                myTarget.whiteList[i] = EditorGUILayout.TextField(myTarget.whiteList[i]);

                if (GUILayout.Button("Remove"))
                {
                    myTarget.whiteList.Remove(myTarget.whiteList[i]);
                    return;
                }

                EditorGUILayout.EndHorizontal();
            }

            if (GUILayout.Button("Add"))
            {
                myTarget.whiteList.Add(string.Empty);
                return;
            }

            myTarget.blackListEnabled = false;
        }

        if (myTarget.blackListEnabled)
        {
            myTarget.blackListEnabled = EditorGUILayout.Toggle("Blacklist", myTarget.blackListEnabled);

            for (int i = 0; i < myTarget.blackList.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                myTarget.blackList[i] = EditorGUILayout.TextField(myTarget.blackList[i]);

                if (GUILayout.Button("Remove"))
                {
                    myTarget.blackList.Remove(myTarget.blackList[i]);
                    return;
                }

                EditorGUILayout.EndHorizontal();
            }

            if (GUILayout.Button("Add"))
            {
                myTarget.blackList.Add(string.Empty);
                return;
            }

            myTarget.whitelistEnabled = false;
        }
    }
}
#endif
