﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAnimationController : MonoBehaviour {

    public bool looping;
    public Sprite[] frames;

    [SerializeField] private SpriteRenderer _renderer;

    private int _index;

    public void NextFrame()
    {
        if (looping)
        {
            _index++;

            if (_index > frames.Length - 1)
            {
                _index = 0;
            }
        }
        else
        {
            _index = Mathf.Clamp(_index++, 0, frames.Length - 1);
        }

        _renderer.sprite = frames[_index];
    }

    public void PreviousFrame()
    {
        if (looping)
        {
            _index--;

            if (_index < 0)
            {
                _index = frames.Length - 1;
            }
        }
        else
        {
            _index = Mathf.Clamp(_index--, 0, frames.Length-1);
        }

        _renderer.sprite = frames[_index];
    }
}
