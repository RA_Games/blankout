﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClockObject : MonoBehaviour {

    public bool startOnAwake = true;
    public UnityEvent onTick;

    [Range(0,120)]
    public float waitTime = 1;

    private float delayTimeInSeconds;

    private void Awake()
    {
        if (startOnAwake)
            StartClock();
        else StopClock();
    }

    public void StartClock()
    {
        SetTimer();
        enabled = true;
    }

    public void StopClock()
    {
        enabled = false;
    }

    private void Update()
    {
        if (Time.time > delayTimeInSeconds)
        {
            onTick.Invoke();
            SetTimer();
        }
    }

    private void SetTimer()
    {
        delayTimeInSeconds = Time.time + waitTime;
    }

}
