﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public Transform maxUp,maxDown;
    public Transform pivotUp, pivotDown;
    public SpriteRenderer rope;
    public Transform pulleyPos;

    public List<BigBox> boxes = new List<BigBox>();

    public Box.Material material;
   
    private void Awake()
    {
        foreach (var box in boxes)
        {
            if (box.currentMaterial != material)
            {
                box.gameObject.SetActive(false);
            }
        }       
        
    }

    public bool CanUp(float velocity)
    {
        //print(pivotUp.position.y +" < "+ maxUp.position.y);
        if(pivotUp.position.y /*+ velocity*/ < maxUp.position.y )
        {
            return true;
        }
        return false;
    }

    public bool CanDown(float velocity)
    {
        if (pivotDown.position.y /*- velocity*/ > maxDown.position.y)
        {
            return true;
        }
        return false;
    }

    public float GetWeight()
    {
        float aux = 0;
        foreach (var box in boxes)
        { 
            if(box.gameObject.activeInHierarchy)
            {
                aux += box.Rb.mass;
            }
        }
        return aux;
    }

    private void RecursiveCollision(Rigidbody2D rb)
    {
        /*
        if (rb == null) return;

        var aux = new Collider2D[10];

        if (!overPlatform.Contains(rb))
        {
            overPlatform.Add(rb);
            
            rb.OverlapCollider(new ContactFilter2D(), aux);
            for (int i = 0; i < aux.Length; i++)
            {
                if (aux[i] != null)
                {
                    RecursiveCollision(aux[i].attachedRigidbody);
                }
            }

        }
        */
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        //RecursiveCollision(other.rigidbody);
    }

    // Update is called once per frame
    void Update ()
    {
        //print("size rope:"+ (pulleyPos.position.y - rope.transform.position.y));
        rope.size = new Vector2(rope.size.x, pulleyPos.position.y - rope.transform.position.y);
	}
}
