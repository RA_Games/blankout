﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulleyManager : MonoBehaviour
{
   public Platform platformLeft, platformRigth;

    void Start ()
    {

	}
	
	void Update ()
    {
        //asdf();
        float difMass = platformLeft.GetWeight() - platformRigth.GetWeight();
        difMass /= 100;

        if (difMass > 0)
        {
            //print("A: " + platformRigth.CanDown(difMass * Time.deltaTime) + " && " + platformLeft.CanUp(difMass * Time.deltaTime) +" vel: "+ difMass * Time.deltaTime);
            if(platformRigth.CanUp(difMass) && platformLeft.CanDown(difMass))
            {
                platformLeft.transform.Translate(new Vector3(0, -difMass, 0) * Time.deltaTime);
                platformRigth.transform.Translate(new Vector3(0, difMass, 0) * Time.deltaTime);
            }
        }
        else
        {
            //print("B: " + platformRigth.CanUp(difMass) + " && " + platformLeft.CanDown(difMass));
            if (platformRigth.CanDown(difMass) && platformLeft.CanUp(difMass))
            {
                platformLeft.transform.Translate(new Vector3(0, -difMass, 0) * Time.deltaTime);
                platformRigth.transform.Translate(new Vector3(0, difMass, 0) * Time.deltaTime);
            }
        }


    }

    private void asdf()
    {
        /*
        foreach (Rigidbody2D rb in platforms[0].overPlatform)
        {
            diference += rb.mass;
        }

        foreach (Rigidbody2D rb in platforms[1].overPlatform)
        {
            diference -= rb.mass;
        }

        diference /= reduction;
        print("dif: " + diference);

        if (
            platforms[0].gameObject.transform.position.y - diference * Time.deltaTime >= platforms[0].maxPosition.position.y &&
            platforms[1].gameObject.transform.position.y + diference * Time.deltaTime >= platforms[1].maxPosition.position.y &&
            platforms[0].gameObject.transform.position.y - diference * Time.deltaTime <= platforms[0].minPosition.position.y &&
            platforms[1].gameObject.transform.position.y + diference * Time.deltaTime <= platforms[1].minPosition.position.y
            )
        {

            pulleyAnims[0].speed = 1;
            pulleyAnims[1].speed = 1;

            platforms[0].gameObject.transform.Translate(new Vector2(0, -diference * Time.deltaTime));
            platforms[1].gameObject.transform.Translate(new Vector2(0, diference * Time.deltaTime));
        }
        else
        {
            pulleyAnims[0].speed = 0;
            pulleyAnims[1].speed = 0;
        }


    */

    }
}
