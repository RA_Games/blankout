﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBox : Box
{
    public Pedestal.Figure figure;
    [SerializeField] private Animator figure_anim;

    private void Start()
    {
        figure_anim.SetTrigger(figure.ToString());
    }

    public override void Init()
    {
        if (figure == Pedestal.Figure.None)
        {
            figure_anim.gameObject.SetActive(false);
        }
    }
}
