﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.U2D;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class Box : MonoBehaviour {

    public enum Material
    {
        WOOL, WOOD, CLAY, ROCK, METAL, MAGNETIC 
    }

    public MagnetCharge currentCharge;
    public MagnetCharge originalCharge;
    public Material currentMaterial;
    public Material originalMaterial;

    private Rigidbody2D _rb;
    public Rigidbody2D Rb
    {
        get
        {
            if (_rb == null)
                _rb = GetComponent<Rigidbody2D>();

            return _rb;
        }
    }

    [Space]
    public UnityEvent onWood;
    public UnityEvent onRock;
    public UnityEvent onMetal;
    public UnityEvent onMagnetNeg;
    public UnityEvent onMagnetPos;

    public float Weight
    {
        get
        {
            if(_rb == null)
                _rb = GetComponent<Rigidbody2D>();

            return _rb.mass;
        }
    }

    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        ChangueMaterial(currentMaterial);
        Init();
    }

    public abstract void Init();

    public void ChangueMaterial(Material newMat)
    {
        foreach (Transform child in transform)
        {
            if(child.GetComponent<Animator>() == null)
                child.gameObject.SetActive(false);
        }

        currentMaterial = newMat;

        switch (newMat)
        {
            case Material.WOOD:
                onWood.Invoke();
                break;
            case Material.ROCK:
                onRock.Invoke();
                break;
            case Material.METAL:
                onMetal.Invoke();
                break;
            case Material.MAGNETIC:
                if (currentCharge == MagnetCharge.NEGATIVE)
                    onMagnetNeg.Invoke();
                else if (currentCharge == MagnetCharge.POSITIVE)
                    onMagnetPos.Invoke();
                break;
        }
    }

    public void SetCharge(MagnetCharge c)
    {
        currentCharge = c;
    }
}
