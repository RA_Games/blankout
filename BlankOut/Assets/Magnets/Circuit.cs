﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(Tilemap))]
public class Circuit : MonoBehaviour {

    public ElectricPedestal electricPedestal;
    Tilemap tilemap;

	// Use this for initialization
	void Start ()
    {
        tilemap = gameObject.GetComponent<Tilemap>();
        tilemap.GetComponent<Renderer>().material.color = Color.gray;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(electricPedestal.energy != 0)
        {
            tilemap.GetComponent<Renderer>().material.color = Color.white;
        }
        else
        {
            Color myColor = new Color(66/256f,66/256f,66/256f,0.8f);

            //ColorUtility.TryParseHtmlString("#09FF0064", out myColor);
            tilemap.GetComponent<Renderer>().material.color = myColor;
            
        }
	}
}
