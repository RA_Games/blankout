﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricPedestal : MonoBehaviour {

    public int energy = 0;
    public GameObject anim;
    public bool active;

    private void Start()
    {
        active = false;
        anim.SetActive(active);
    }

    private void Update()
    {
        if(energy > 0)
        {
            if(!active)
            {
                active = true;
                anim.SetActive(active);
            }
        }
        else
        {
            if(active)
            {
                active = false;
                anim.SetActive(active);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        EnergyPlant p = collision.gameObject.GetComponent<EnergyPlant>();
        if (p != null)
            energy += p.height;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        EnergyPlant p = collision.gameObject.GetComponent<EnergyPlant>();
        if (p != null)
            energy -= p.height;
    }
}
