﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticPedestal : MonoBehaviour {

    private int charge;
    public List<Box> boxes;
    public Circuit circuit;
    private float magnetForce;
    public MagnetDirection direction;
    public MagnetCharge magnetCharge;
    private Vector3 offset;
    public GameObject pos;
    public GameObject neg;
    public GameObject neutral;
    public GameObject anim;


	// Use this for initialization
	void Start ()
    {
        if (magnetCharge == MagnetCharge.NEGATIVE)
            neg.SetActive(true);
        else if (magnetCharge == MagnetCharge.POSITIVE)
            pos.SetActive(true);
        else
            neutral.SetActive(true);
        magnetForce = 9.8f * 320;
        boxes = new List<Box>();
        switch(direction)
        {
            case MagnetDirection.UP: offset = new Vector3(0,-0.5f,0) ; break;
            case MagnetDirection.DOWN: offset = new Vector3(0, 0.5f, 0) ; break;
            case MagnetDirection.RIGTH: offset = new Vector3(-0.5f, 0, 0) ; break;
            case MagnetDirection.LEFT: offset = new Vector3(0.5f, 0, 0) ; break;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        anim.SetActive(circuit.electricPedestal.active);
	}

    private void FixedUpdate()
    {
        foreach (Box b in boxes)
        {
            if (b.currentCharge != magnetCharge)
                charge = 1;
            else
                charge = -1;
            Vector2 distance;
            if (b.GetComponent<SmallBox>() != null)
                distance = (transform.position - b.transform.position - offset / 2);
            else
                distance = (transform.position - b.transform.position - offset);

            float x = 0;
            float y = 0;

            if (Mathf.Abs(distance.y) < 0.5f)
            {
                if (distance.y == 0)
                    distance.y = 0.1f;
                distance.y = distance.y * 0.5f / Mathf.Abs(distance.y);
            }

            if (Mathf.Abs(distance.x) < 0.5)
            {
                if (distance.x == 0)
                    distance.x = 0.1f;
                distance.x = distance.x * 0.5f / Mathf.Abs(distance.x);
            }

            if (direction == MagnetDirection.UP || direction == MagnetDirection.DOWN)
            {
                if(Mathf.Abs(distance.x) > 0.5)
                {
                    x = 0;
                }
                y = charge * (distance.y / Mathf.Abs(distance.y)) * circuit.electricPedestal.energy * magnetForce;
            }
            else
            {
                if (Mathf.Abs(distance.y) > 0.5)
                {
                    y = 0;
                }
                x = charge * ((distance.x / Mathf.Abs(distance.x))) * circuit.electricPedestal.energy * magnetForce;
            }

            x = x / (distance.x * distance.x);
            y = y / (distance.y * distance.y);
            

            b.GetComponent<Rigidbody2D>().AddForce(new Vector2(x,y));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Box b = collision.gameObject.GetComponent<Box>();
        if (b != null)
        {
            if (b.currentMaterial == Box.Material.METAL || b.currentMaterial == Box.Material.MAGNETIC)
                if (!boxes.Contains(b))
                    boxes.Add(b);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Box b = collision.gameObject.GetComponent<Box>();
        if (b != null)
        {
            if (b.currentMaterial == Box.Material.METAL || b.currentMaterial == Box.Material.MAGNETIC)
                if (boxes.Contains(b))
                    boxes.Remove(b);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Box b = collision.gameObject.GetComponent<Box>();
        if (b != null)
        {
            if (b.currentMaterial == Box.Material.METAL  || b.currentMaterial == Box.Material.MAGNETIC)
            {
                if (!boxes.Contains(b))
                {
                    boxes.Add(b);
                }
            }
            else
            {
                if (boxes.Contains(b))
                {
                    boxes.Remove(b);
                }
            }
        }
    }
}
